chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);
			
					var rand = [];
					var open = [];
					var timeout = [];
					var j=0;

					//get parameter value from a string 
					function getParameterByName(name) {
					    
					    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
					    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
					        results = regex.exec(location.search);
					    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
					}

					//check link type in order to underestand does a person is using a custom url 
					function link_type(link){
						
						var check=link.search('/in/'); 
						var app_indentifier='';
						
						if(check!=-1){
							app_indentifier='?linkedin-helper=true'
						}
						else{
							app_indentifier='&linkedin-helper=true'
						}

						return app_indentifier;
					}

					// go trought google results

			    	$('cite._Rm').each(function(i, items_list){
					   
					    var link=$(items_list).html();
					   
					    rand[j] = Math.floor((Math.random() * 60) + 60)*1000
						timeout[j] = (timeout[j-1]) ? rand[j] + timeout[j-1]: 1000;
						console.log(timeout[j]);
						open[j] = link+link_type(link)
				
					    setTimeout(window.open, timeout[j], open[j]);
					    j++;

					});

		        	//next page iterations 
		        	var page_number = getParameterByName('start');
		        	
		        	//only 3 page limit
		        	if(page_number<30){
		        		var next_page='https://www.google.com'+$('.pn').attr('href')+'&linkedin-helper=true';
						setTimeout(function(){window.open(next_page,'_blank')},300000)
					}
		}
	}, 10);
});