chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
        clearInterval(readyStateCheckInterval);

		var type = "event",
			api_key = "2f138432c6e44155021c59a21527",
			page = 1,
			event_id = "224882263",
		    group_id = "San-Francisco-Project-Management-Meetup",
			offset = page - 1;

		if (type == "group") {

			$.getJSON( "https://api.meetup.com/2/members?&sign=true&photo-host=public&group_urlname="+group_id+"&page=200&offset="+offset+"&sign=true&key="+api_key, function() {
			  	console.log( "success" );
			}).done(function(data) {

				//console.log(data);
				if (data.results.length) {
				//	console.log(data);
				//	console.log(data.results.length);


					var members_array=[];
					$.each(data.results,function(key,value){

						var member_name=value.name;
						console.log(value.name)
						var member_id=value.id;
						members_array.push({'member_id':member_id,'member_name':member_name});
					})
					var obj={};

					obj['json']= JSON.stringify(members_array);

					chrome.storage.local.set(obj);

				};

			}).fail(function(data) {
				console.log( "error" );
				console.log(data);
			}).always(function() {
				console.log( "complete" );
			});

		} else {

			$.getJSON( "https://api.meetup.com/2/rsvps?&sign=true&photo-host=public&event_id="+event_id+"&page=200&offset="+offset+"&sign=true&key="+api_key, function() {
			  	console.log( "success" );
			}).done(function(data) {
				if (data.results.length) {


					var members_array=[];
					$.each(data.results,function(key,value){

						var member_name=value.member.name;
						var member_id=value.member.member_id;
						members_array.push({'member_id':member_id,'member_name':member_name});
					})
					var obj={};
					obj['json']= JSON.stringify(members_array);
					chrome.storage.local.set(obj);

				};

			}).fail(function(data) {
				console.log( "error" );
				console.log(data);
			}).always(function() {
				console.log( "complete" );
			});
		}
	}
	}, 10);
});