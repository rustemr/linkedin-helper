chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);

		var name = getUrlVars()["name"],
			names = name.split("+"),
			firstname = (names[0].length) ? names[0].trim(): name.trim(),
			lastname = (typeof names[1] != "undefined") ? names[1].trim():'';

			console.log(name)
			console.log(names)

			setTimeout(function(){
				$('.convoItem').each( function(){
					console.log($(this).attr("title"))
					var convo_name = $(this).attr("title").trim().split(" "),
						convo_firstname = (names[0].length) ? names[0].trim(): name.trim(),
						convo_lastname = (typeof names[1] != "undefined") ? names[1].trim():'';
					if ( convo_firstname === firstname && convo_lastname === lastname) {
						window.close();
					}
				});
			}, 3000);

			$('.composeBox-textArea').html('Hi '+capitalizeFirstLetter(firstname)+',\nI saw that you are also a member of the Data Science meetup group and you went to an event at OpenTable. Let\'s keep in touch!. I\'m also looking for opportunities within Business and Data Analysis. Please, let me know if you hear about any opportunities.\nThanks,\n Rustem Ramazanov \nLinkedIn: http://bit.ly/linkedin_rustemr \nResume: http://bit.ly/resume_rustemr');

			$('#messaging-new-send').removeAttr("disabled");
			$('#messaging-new-send').removeClass("disabled");

			$('#messaging-new-send').trigger('click');
		 	//window.close();
	}
	}, 10);
});

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}